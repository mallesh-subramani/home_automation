/*Mac address of the Tank Node MCU is :- EC:FA:BC:BC:9A:B6
 * Ip address :- 192.168.0.112:80
 */

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include "Ultrasonic.h"

#ifndef STASSID
#define STASSID "SRINIVASA RAMANUJAN_nomap"
#define STAPSK  "Ramanujan22121887"
#endif
long AlarmValue = 60;   //Value is in %
float TankHeight = 300;     //Value is in cm
float WaterLevel_Percentage;
long averaging_value =100;
long WaterLevel;
const char* ssid = STASSID;
const char* password = STAPSK;
float RangeInCentimeters;
float RangeInCentimeters_filter = 0;
//long RangeInMeters;
String Indicator = "Tank Full";
String Status;
Ultrasonic ultrasonic(0);   //0 related to D3 on Node MCU

ESP8266WebServer server(80);

const int led = 13;

void handleRoot() {
  digitalWrite(led, 1);
  server.send(400, "text/plain", "WaterLevel = "+String(WaterLevel_Percentage)+"% "+ "\n"+String(Status));
  digitalWrite(led, 0);
}

void handleNotFound() {
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}

void setup(void) {
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "this works as well");
  });

  server.on("/gif", []() {
    static const uint8_t gif[] PROGMEM = {
      0x47, 0x49, 0x46, 0x38, 0x37, 0x61, 0x10, 0x00, 0x10, 0x00, 0x80, 0x01,
      0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0x2c, 0x00, 0x00, 0x00, 0x00,
      0x10, 0x00, 0x10, 0x00, 0x00, 0x02, 0x19, 0x8c, 0x8f, 0xa9, 0xcb, 0x9d,
      0x00, 0x5f, 0x74, 0xb4, 0x56, 0xb0, 0xb0, 0xd2, 0xf2, 0x35, 0x1e, 0x4c,
      0x0c, 0x24, 0x5a, 0xe6, 0x89, 0xa6, 0x4d, 0x01, 0x00, 0x3b
    };
    char gif_colored[sizeof(gif)];
    memcpy_P(gif_colored, gif, sizeof(gif));
    // Set the background to a random set of colors
    gif_colored[16] = millis() % 256;
    gif_colored[17] = millis() % 256;
    gif_colored[18] = millis() % 256;
    server.send(200, "image/gif", gif_colored, sizeof(gif_colored));
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  
  /*Serial.print("MAC: ");                
  Serial.println(WiFi.macAddress());          //Used to print the device MAc address
  */
  RangeInCentimeters = ultrasonic.MeasureInCentimeters();
  delay(250);
  for(int i=1;i <= averaging_value;i++)
  {
      if(i == 1)
      {   
        RangeInCentimeters_filter = 0;
      }
      RangeInCentimeters_filter+=RangeInCentimeters;
  }
  //RangeInMeters = RangeInCentimeters/100;
  WaterLevel = TankHeight - (RangeInCentimeters_filter/averaging_value);
  if(WaterLevel >= 0)
  {
      WaterLevel_Percentage = (WaterLevel/TankHeight)*100;
  }
  if(WaterLevel_Percentage >= AlarmValue)
    {   Status = Indicator;
    }
   else
    { 
      Status = " ";
    }
  Serial.print("Percentage: ");
  Serial.println(WaterLevel_Percentage);
  Serial.print("Centimeters: ");
  Serial.println(RangeInCentimeters);
  delay(250);
  server.handleClient();
  MDNS.update();
}
